package org.toolbox.sort;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.toolbox.sort.SortUtils.shuffle;

import java.util.Arrays;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class AppTest {

	public static final int ARR_SIZE = 1_000_000;

	@SuppressWarnings("unchecked")
	public static final Sorter<Integer>[] sorters = new Sorter[] {
		// new BubbleSorter(),
		// new InsertionSorter<Integer>(Integer::compare),
		// new InsertionArraySorter<Integer>(Integer::compare),
		// new SelectionSorter<Integer>(Integer::compare),
		// new HeapSorter<Integer>(Integer::compare),
		new QuickSorter<Integer>(Integer::compare),
		new MergeSorter<Integer>(Integer::compare),
		new DefaultSorter<Integer>(Integer::compare),
		// new CountingSorter(ARR_SIZE),
		// new RadixSort(10, ARR_SIZE),
		// new RadixBucketSorter(10, ARR_SIZE)
	};

	public static Integer[] buildArray(int len) {
		Integer[] res = new Integer[len];
		for(int i = 0; i < len; i++)
			res[i] = i;

		return res;
	}

	@Disabled
	@Test
	public void testCountingMapperSort() {

		System.out.println("################################################");
		System.out.println("COUNTING BUCKET SORTER TEST");

		Integer[] arr = buildArray(ARR_SIZE);

		shuffle(arr);

		printlnFirstElems(arr, 100);

		CountingBucketSorter<Integer> sorter = new CountingBucketSorter<>(ARR_SIZE);
		sorter.sort(arr, i -> i / 10);

		printlnFirstElems(arr, 100);

		assertSorted(arr);
	}

	@Disabled
	@Test
	public void testRadixSortPerformance() {

		int baseLimit = 1025;
		int runs = 100;

		Integer[] arr = buildArray(ARR_SIZE);
		shuffle(arr);

		double min = Double.MAX_VALUE;
		int minBase = -1;

		System.out.println("################################################");
		System.out.println("RADIX PERFORMANCE FOR BASE TESTS");
		System.out.printf("AVG OF %d RUNS\n", runs);
		for(int i = 5; i < baseLimit; i += 5) {
			Sorter<Integer> radix = new RadixSort(i, ARR_SIZE);

			double sum = 0;
			for(int run = 0; run < runs; run++) {
				Integer[] cpy = Arrays.copyOf(arr, arr.length);
				long start = System.nanoTime();
				radix.sort(cpy);
				long runtime = System.nanoTime() - start;

				sum += runtime / 1_000_000d;
			}

			double avg = sum / runs;
			if(avg < min) {
				min = avg;
				minBase = i;
			}

			System.out.printf("RADIX FOR BASE %d: %f\n", i, sum / runs);
		}

		System.out.printf("BEST TIME FOR BASE %d [ %f ]\n", minBase, min);
	}

	// @Disabled
	@Test
	public void testSort() {
		Integer[] arr = buildArray(ARR_SIZE);
		shuffle(arr);

		for(var s: sorters)
			testSorter(s, Arrays.copyOf(arr, arr.length));
	}

	public void testSorter(Sorter<Integer> sorter, Integer[] arr) {

		System.out.println("################################################");
		System.out.println("TESTING " + sorter);

		System.out.print("BEFORE SORT:\t");
		printlnFirstElems(arr, 100);

		long start = 0;
		long runtime;
		try {
			start = System.nanoTime();
			sorter.sort(arr);
			runtime = System.nanoTime() - start;

		} catch(Throwable e) {
			runtime = System.nanoTime() - start;
			System.out.println("SORTER FAILED: " + e);
			e.printStackTrace();
		}

		System.out.print("AFTER SORT:\t");
		printlnFirstElems(arr, 100);

		System.out.printf("RUNTIME FOR %d ELEM: %010.4f ms\n",
				arr.length,
				runtime / 1_000_000d);

		for(int i = 1; i < arr.length; i++)
			assertTrue(arr[i - 1] <= arr[i], () -> String.format("Sorter %s failed", sorter));
	}

	private void printlnFirstElems(Object[] arr, int n) {
		System.out.print("[");
		int i;
		for(i = 0; i < Math.min(arr.length, n); i++)
			System.out.print(" " + arr[i]);

		if(i != arr.length)
			System.out.print(" ...");

		System.out.println(" ]");
	}

	private void assertSorted(Integer[] arr) {
		for(int i = 1; i < arr.length; i++)
			assertTrue(arr[i - 1] <= arr[i]);
	}
}
