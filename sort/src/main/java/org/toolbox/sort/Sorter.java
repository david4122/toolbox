package org.toolbox.sort;

public interface Sorter<T> {

	public void sort(T[] arr);
}
