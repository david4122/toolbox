package org.toolbox.sort;

import java.util.Arrays;
import java.util.Comparator;

public class MergeSorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public MergeSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	@Override
	public void sort(T[] arr) {
		merge(Arrays.copyOf(arr, arr.length), arr, 0, arr.length, comp);
	}

	private void merge(T[] src, T[] dst, int start, int end, Comparator<T> comp) {
		if(end - start <= 1)
			return;

		int m = (start + end) / 2;

		merge(dst, src, start, m, comp);
		merge(dst, src, m, end, comp);

		int i1 = start, i2 = m;
		for(int i = start; i < end; i++) {
			if(i2 >= end || (i1 < m && comp.compare(src[i1], src[i2]) <= 0)) {
				dst[i] = src[i1++];
			} else {
				dst[i] = src[i2++];
			}
		}
	}
}
