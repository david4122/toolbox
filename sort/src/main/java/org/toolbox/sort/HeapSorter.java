package org.toolbox.sort;

import java.util.Comparator;

public class HeapSorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public HeapSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	private static class Heap<T> {
		private Object[] heap;
		private int size = 0;
		private Comparator<T> comp;

		Heap(int capacity, Comparator<T> comp) {
			heap = new Object[capacity];
			this.comp = comp;
		}

		void offer(T elem) {
			int i = size++;

			while(i > 0 && comp.compare(elem, get(parent(i))) < 0) {
				heap[i] = get(parent(i));

				i = parent(i);
			}
			heap[i] = elem;
		}

		T poll() {
			T min = get(0);

			T last = get(--size);
			int i = 0;
			int c = smallerChild(i);
			while(c > 0 && comp.compare(last, get(c)) > 0) {
				heap[i] = get(c);

				i = c;
				c = smallerChild(i);
			}
			heap[i] = last;

			return min;
		}

		private int smallerChild(int p) {
			if(left(p) >= size)
				return -1;
			if(right(p) >= size)
				return left(p);

			return comp.compare(leftVal(p), rightVal(p)) < 0 ? left(p) : right(p);
		}

		@SuppressWarnings("unchecked")
		private T get(int index) {
			return (T) heap[index];
		}

		private int parent(int index) {
			return (index - 1) / 2;
		}

		private int left(int index) {
			return index * 2 + 1;
		}

		private int right(int index) {
			return index * 2 + 2;
		}

		private T leftVal(int index) {
			return get(left(index));
		}

		private T rightVal(int index) {
			return get(right(index));
		}
	}

	@Override
	public void sort(T[] arr) {
		Heap<T> heap = new Heap<>(arr.length, comp);

		for(T i: arr)
			heap.offer(i);

		for(int i = 0; i < arr.length; i++)
			arr[i] = heap.poll();
	}
}
