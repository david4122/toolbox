package org.toolbox.sort;

import static org.toolbox.sort.SortUtils.swap;

import java.util.Comparator;

public class BubbleSorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public BubbleSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	@Override
	public void sort(T[] arr) {
		boolean change = true;

		while(change) {
			change = false;

			for(int i = 1; i < arr.length; i++) {
				if(comp.compare(arr[i - 1], arr[i]) > 0) {
					swap(arr, i - 1, i);
					change = true;
				}
			}
		}
	}
}
