package org.toolbox.sort;

import java.util.Arrays;
import java.util.Comparator;

public class DefaultSorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public DefaultSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	@Override
	public void sort(T[] arr) {
		Arrays.sort(arr, comp);
	}
}
