package org.toolbox.sort;

import java.util.Arrays;
import java.util.function.Function;

public class RadixBucketSorter implements Sorter<Integer> {

	class Queue<T> {

		private static class Node<N> {
			N val;
			Node<N> next;

			Node(N val) {
				this.val = val;
			}

			Node(N val, Node<N> next) {
				this(val);
				this.next = next;
			}
		}

		private Node<T> head;
		private Node<T> tail;

		private int size = 0;

		public void offer(T e) {
			size++;

			if(null == head) {
				head = tail = new Node<>(e);
				return;
			}

			tail.next = new Node<>(e);
			tail = tail.next;
		}

		public T poll() {
			if(null == head)
				return null;

			size--;

			T e = head.val;
			head = head.next;
			return e;
		}

		public int size() {
			return size;
		}

		public boolean isEmpty() {
			return size == 0;
		}
	}

	private int base;
	private int maxLength;

	public RadixBucketSorter(int base, int maxVal) {
		this.base = base;
		this.maxLength = len(maxVal);
	}

	@Override
	public void sort(Integer[] arr) {
		Queue<Integer>[] buckets = buildBuckets();
		Queue<Integer>[] temp = buildBuckets();

		sortInto(arr, buckets, digit(0));

		for(int d = 1; d < maxLength; d++) {
			carryOver(buckets, temp, digit(d));

			var swp = buckets;
			buckets = temp;
			temp = swp;
		}

		writeInto(buckets, arr);
	}

	private Queue<Integer>[] sortInto(
			Integer[] arr,
			Queue<Integer>[] buckets,
			Function<Integer, Integer> mapper) {

		for(int i = 0; i < buckets.length; i++)
			buckets[i] = new Queue<>();

		for(var i: arr)
			buckets[mapper.apply(i)].offer(i);

		return buckets;
	}

	private void writeInto(Queue<Integer>[] buckets, Integer[] arr) {
		int arri = 0;
		for(var bucket: buckets) {
			while(!bucket.isEmpty())
				arr[arri++] = bucket.poll();
		}
	}

	private void carryOver(
			Queue<Integer>[] srcs,
			Queue<Integer>[] dsts,
			Function<Integer, Integer> mapper) {

		for(var src: srcs) {
			Integer e;
			while(null != (e = src.poll()))
				dsts[mapper.apply(e)].offer(e);
		}
	}

	private Queue<Integer>[] buildBuckets() {
		Queue<Integer>[] buckets = new Queue[base];

		for(int i = 0; i < buckets.length; i++)
			buckets[i] = new Queue<>();

		return buckets;
	}

	private Function<Integer, Integer> digit(int d) {
		return i -> digit(i, d);
	}

	private int digit(int n, int d) {
		while(d-- > 0 && n > 0)
			n /= base;

		return n % base;
	}

	private int len(int n) {
		int cnt = 1;
		while(n > 0) {
			n /= base;
			cnt++;
		}
		return cnt;
	}
}
