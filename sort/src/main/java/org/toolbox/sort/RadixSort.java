package org.toolbox.sort;

import static org.toolbox.sort.SortUtils.copyInto;

public class RadixSort implements Sorter<Integer> {

	private int base;
	private int max;

	public RadixSort(int base, int max) {
		this.base = base;
		this.max = max;
	}

	@Override
	public void sort(Integer[] arr) {
		for(int i = 0; i < len(max); i++)
			countingSort(arr, i);
	}

	private void countingSort(Integer[] arr, int digit) {
		copyInto(CountingSorter.countingSort(
					arr,
					base,
					i -> digit(i, digit),
					new Integer[arr.length]),
				arr);
	}

	private int digit(int x, int d) {
		for(int i = 0; i < d; i++) 
			x /= base;

		return x % base;
	}

	private int len(int n) {
		int c = 0;
		while(n > 0) {
			n /= base;
			c++;
		}

		return c;
	}
}
