package org.toolbox.sort;

import java.util.Random;

public class SortUtils {

	private static Random rand = new Random();

	public static <T> void swap(T[] arr, int a, int b) {
		if(a == b)
			return;

		T tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}

	public static <T> void shuffle(T[] arr) {
		for(int i = 0; i < arr.length; i++)
			swap(arr, i, rand.nextInt(arr.length));
	}

	public static <T> void copyInto(T[] src, T[] dst) {
		for(int i = 0; i < dst.length; i++)
			dst[i] = src[i];
	}
}
