package org.toolbox.sort;

import java.util.Comparator;

public class InsertionArraySorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public InsertionArraySorter(Comparator<T> comp) {
		this.comp = comp;
	}

	@Override
	public void sort(T[] arr) {
		for(int i = 1; i < arr.length; i++) {
			int j = i;

			T el = arr[j];
			for(; j > 0 && comp.compare(arr[j - 1], el) > 0; j--) {
				arr[j] = arr[j - 1];
			}

			arr[j] = el;
		}
	}
}
