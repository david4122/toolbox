package org.toolbox.sort;

import static org.toolbox.sort.SortUtils.swap;

import java.util.Comparator;

public class SelectionSorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public SelectionSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	@Override
	public void sort(T[] arr) {
		for(int i = 0; i < arr.length; i++) {
			int min = i;

			for(int j = i + 1; j < arr.length; j++) {
				if(comp.compare(arr[min], arr[j]) > 0)
					min = j;
			}

			swap(arr, i, min);
		}
	}
}
