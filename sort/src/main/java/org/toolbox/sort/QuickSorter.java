package org.toolbox.sort;

import static org.toolbox.sort.SortUtils.swap;

import java.util.Comparator;

public class QuickSorter<T> implements Sorter<T> {

	private Comparator<T> comp;

	public QuickSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	@Override
	public void sort(T[] arr) {
		quicksort(arr, 0, arr.length);
	}

	private void quicksort(T[] arr, int start, int end) {
		if(end - start <= 1)
			return;

		int p = partition(arr, start, end);

		quicksort(arr, start, p);
		quicksort(arr, p, end);
	}

	private int partition(T[] arr, int start, int end) {
		int p = (start + end) / 2;

		int s = start, e = end - 1;
		while(s < e) {
			while(s < p) {
				if(comp.compare(arr[p], arr[s]) < 0) {
					swap(arr, s, p);
					p = s;

					break;
				}

				s++;
			}

			while(e > p) {
				if(comp.compare(arr[p], arr[e]) > 0) {
					swap(arr, s, e);
					p = e;

					break;
				}

				e--;
			}
		}

		return p;
	}
}
