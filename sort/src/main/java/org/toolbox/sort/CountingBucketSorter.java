package org.toolbox.sort;

import java.util.function.Function;

public class CountingBucketSorter<T> implements Sorter<T> {

	class Queue<T> {

		private static class Node<N> {
			N val;
			Node<N> next;

			Node(N val) {
				this.val = val;
			}

			Node(N val, Node<N> next) {
				this(val);
				this.next = next;
			}
		}

		private Node<T> head;
		private Node<T> tail;

		private int size = 0;

		public void offer(T e) {
			size++;

			if(null == head) {
				head = tail = new Node<>(e);
				return;
			}

			tail.next = new Node<>(e);
			tail = tail.next;
		}

		public T poll() {
			if(null == head)
				return null;

			size--;

			T e = head.val;
			head = head.next;
			return e;
		}

		public int size() {
			return size;
		}

		public boolean isEmpty() {
			return size == 0;
		}
	}


	private int max;

	public CountingBucketSorter(int max) {
		this.max = max;
	}

	@Override
	public void sort(T[] arr) {}

	public void sort(T[] arr, Function<T, Integer> mapper) {
		Queue<T>[] buckets = new Queue[max];

		for(T i: arr) {
			int idx = mapper.apply(i);

			if(null == buckets[idx])
				buckets[idx] = new Queue<>();

			buckets[idx].offer(i);
		}

		int arri = 0;
		for(int ci = 0; ci < buckets.length; ci++) {
			while(buckets[ci] != null && !buckets[ci].isEmpty())
				arr[arri++] = buckets[ci].poll();
		}
	}
}
