package org.toolbox.sort;

import static org.toolbox.sort.SortUtils.copyInto;

import java.lang.reflect.Array;
import java.util.function.Function;

public class CountingSorter<T> implements Sorter<T> {

	private int maxValue;
	private int offset;
	private Function<T, Integer> mapper;

	public CountingSorter(int maxValue, int offset, Function<T, Integer> mapper) {
		this.maxValue = maxValue;
		this.offset = offset;
		this.mapper = mapper;
	}

	public CountingSorter(int maxValue, Function<T, Integer> mapper) {
		this(maxValue, 0, mapper);
	}

    @Override
	@SuppressWarnings("unchecked")
    public void sort(T[] arr) {
		T[] sorted = (T[]) Array.newInstance(
				arr.getClass().getComponentType(), arr.length);

		copyInto(
				countingSort(arr, maxValue, offset, mapper, sorted),
				arr);
    }

	public static <S> S[] countingSort(
			S[] arr,
			int maxValue,
			Function<S, Integer> mapper,
			S[] sorted) {

		return countingSort(arr, maxValue, 0, mapper, sorted);
	}

	public static <S> S[] countingSort(
			S[] arr,
			int maxValue,
			int offset,
			Function<S, Integer> mapper,
			S[] sorted) {

		int[] cnt = new int[maxValue - offset];
		for(S i: arr)
			cnt[mapper.apply(i) - offset]++;

		int[] prefixSum = new int[cnt.length];
		prefixSum[0] = cnt[0];
		for(int i = 1; i < cnt.length; i++)
			prefixSum[i] = prefixSum[i - 1] + cnt[i];

		for(int i = arr.length - 1; i >= 0; i--)
			sorted[--prefixSum[mapper.apply(arr[i]) - offset]] = arr[i];

		return sorted;
	}
}
