package org.toolbox.sort;

import java.util.Comparator;

public class InsertionSorter<T> implements Sorter<T> {

	private static class Node<N> {
		N val;

		Node<N> next;

		Node(N val) {
			this.val = val;
		}

		Node(N val, Node<N> next) {
			this.val = val;
			this.next = next;
		}
	}

	private Comparator<T> comp;

	public InsertionSorter(Comparator<T> comp) {
		this.comp = comp;
	}

	private Node<T> insert(Node<T> head, T val) {

		if(comp.compare(val, head.val) < 0) {
			return new Node<>(val, head);
		}

		for(var i = head; i != null; i = i.next) {
			if(i.next == null || comp.compare(val, i.next.val) <= 0) {
				i.next = new Node<>(val, i.next);

				break;
			}
		}

		return head;
	}

	@Override
	public void sort(T[] arr) {
		Node<T> sorted = new Node<>(arr[0]);

		for(int i = 1; i < arr.length; i++)
			// sorted = sorted.insert(arr[i], comp);
			sorted = insert(sorted, arr[i]);

		for(int i = 0; i < arr.length; i++) {
			arr[i] = sorted.val;
			sorted = sorted.next;
		}
	}
}
