package org.toolbox.collections;

import java.util.function.Consumer;

public class HashSet<T> implements MutableSet<T> {

	private static class Node<N> {
		private N val;
		private Node<N> next;

		public Node(N val) {
			this.val = val;
		}

		public Node(N val, Node<N> next) {
			this(val);
			this.next = next;
		}
	}

	private Node<T>[] table;

	private int size = 0;
	private double loadFactor;
	private double growthFactor;

	@SuppressWarnings("unchecked")
	public HashSet(int initCap, double loadFactor, double growthFactor) {
		table = new Node[initCap];
		this.loadFactor = loadFactor;
		this.growthFactor = growthFactor;
	}

	public HashSet() {
		this(16, 0.75, 2);
	}

	@Override
	public T add(T e) {
		if(null == e)
			return null;

		int idx = index(e);
		for(Node<T> it = table[idx]; it != null; it = it.next) {
			if(it.val.equals(e))
				return null;
		}

		table[idx] = new Node<>(e, table[idx]);
		size++;
		checkLoadFactor();

		return e;
	}

	@Override
	public T remove(T e) {
		if(null == e)
			return null;

		int idx = e.hashCode() % table.length;

		var n = table[idx];

		if(null == n)
			return null;

		if(n.val.equals(e)) {
			table[idx] = n.next;
			size--;

			return n.val;
		}

		while(null != n.next) {
			if(n.next.val.equals(e)) {
				T removed = n.next.val;
				n.next = n.next.next;
				size--;

				return removed;
			}

			n = n.next;
		}

		return null;
	}

	@Override
	public boolean contains(T e) {
		if(null == e)
			return false;

		for(Node<T> it = table[index(e)]; it != null; it = it.next) {
			if(it.val.equals(e))
				return true;
		}

		return false;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void clear() {
		size = 0;
		for(int i = 0; i < table.length; i++)
			table[i] = null;
	}

	@Override
	public void forEach(Consumer<T> consumer) {
		for(int i = 0; i < size; i++) {
			var n = table[i];
			while(null != n) {
				consumer.accept(n.val);

				n = n.next;
			}
		}
	}


	private void checkLoadFactor() {
		if(((double) size) / table.length > loadFactor) {
			grow();
		}
	}

	@SuppressWarnings("unchecked")
	private void grow() {
		Node<T>[] newTable = new Node[(int) (table.length * growthFactor)];

		for(int i = 0; i < table.length; i++) {
			var n = table[i];
			Node<T> next;

			while(null != n) {
				next = n.next;

				int idx = index(n.val, newTable);
				n.next = newTable[idx];
				newTable[idx] = n;

				n = next;
			}
		}

		table = newTable;
	}

	private int index(T e) {
		return index(e, table);
	}

	private int index(T e, Node<T>[] table) {
		return (table.length + e.hashCode()) % table.length;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("HashSet [");
		for(int i = 0; i < table.length; i++) {

			sb.append("[");

			var n = table[i];

			while(null != n) {
				sb.append(" ").append(n.val);

				n = n.next;
			}

			sb.append(" ]");
		}

		sb.append("]");

		return sb.toString();
	}
}
