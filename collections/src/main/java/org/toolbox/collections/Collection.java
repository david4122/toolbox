package org.toolbox.collections;

import java.util.function.Consumer;

public interface Collection<T> {

	boolean contains(T e);

	int size();

	default boolean isEmpty() {
		return 0 == size();
	}

	void forEach(Consumer<T> consumer);
}
