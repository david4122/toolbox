package org.toolbox.collections;

public interface UnindexedCollection<T> extends Collection<T> {}
