package org.toolbox.collections;

import java.util.function.BiConsumer;

public interface IndexedCollection<K, V> extends Collection<V> {

	V get(K key);

	K indexOf(V val);

	boolean containsKey(K key);

	void forEachIndexed(BiConsumer<K, V> consumer);
}
