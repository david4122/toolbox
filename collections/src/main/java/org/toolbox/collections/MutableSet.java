package org.toolbox.collections;

public interface MutableSet<T> extends Set<T>, MutableUnindexedCollection<T> {}
