package org.toolbox.collections;

public interface Queue<T> extends MutableUnindexedCollection<T> {

	default T offer(T e) {
		return add(e);
	}

	T poll();

	T head();
}
