package org.toolbox.collections;

public interface MutableUnindexedCollection<T> extends UnindexedCollection<T>, MutableCollection<T> {

	T add(T e);
}
