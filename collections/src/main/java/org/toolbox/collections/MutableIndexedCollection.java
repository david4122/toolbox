package org.toolbox.collections;

public interface MutableIndexedCollection<K, V> extends IndexedCollection<K, V>, MutableCollection<V> {

	V insert(K key, V val);

	V set(K key, V val);

	V removeAt(K key);
}
