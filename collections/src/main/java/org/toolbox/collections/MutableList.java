package org.toolbox.collections;

public interface MutableList<T> extends List<T>, MutableUnindexedCollection<T>, MutableIndexedCollection<Integer, T> {}
