package org.toolbox.collections;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ArrayList<T> implements MutableList<T> {

	private Object[] array;
	private int size = 0;

	private double growthFactor;

	public ArrayList(int initCap, double growthFactor) {
		array = new Object[initCap];
		this.growthFactor = growthFactor;
	}

	@Override
	public T set(Integer key, T val) {
		T old = get(key);
		if(null == old)
			return null;

		array[key] = val;

		return old;
	}

	@Override
	@SuppressWarnings("unchecked")
	public T get(Integer key) {
		if(!containsKey(key))
			return null;

		return (T) array[key];
	}

	@Override
	public T add(T e) {
		if(size == array.length)
			grow();

		array[size++] = e;

		return e;
	}

	@Override
	public T insert(Integer key, T val) {
		if(!containsKey(key)) {
			if(key == size)
				return add(val);

			return null;
		}

		if(size == array.length)
			grow();

		shiftRight(key);
		array[key] = val;

		size++;

		return val;
	}

	@Override
	public T removeAt(Integer key) {
		T removed = get(key);
		if(null != removed) {
			shiftLeft(key);
			size--;
		}

		return removed;
	}

	@Override
	public T remove(T e) {
		int idx = indexOf(e);
		if(idx < 0)
			return null;

		T removed = get(idx);
		shiftLeft(idx);

		size--;

		return removed;
	}

	@Override
	public boolean containsKey(Integer key) {
		return null != key && key >= 0 && key < size;
	}

	@Override
	public boolean contains(T e) {
		return indexOf(e) != -1;
	}

	@Override
	public Integer indexOf(T e) {
		for(int i = 0; i < size; i++) {
			if(array[i].equals(e))
				return i;
		}

		return -1;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void clear() {
		size = 0;
		for(int i = 0; i < size; i++)
			array[i] = null;
	}

	@Override
	public void forEach(Consumer<T> consumer) {
		for(int i = 0; i < size; i++)
			consumer.accept(get(i));
	}

	@Override
	public void forEachIndexed(BiConsumer<Integer, T> consumer) {
		for(int i = 0; i < size; i++)
			consumer.accept(i, get(i));
	}


	private void grow() {
		Object[] newArray = new Object[(int) (array.length * growthFactor)];

		for(int i = 0; i < array.length; i++)
			newArray[i] = array[i];

		this.array = newArray;
	}

	private void shiftRight(int offset) {
		for(int i = size; i > offset; i--) {
			array[i] = array[i - 1];
		}
	}

	private void shiftLeft(int offset) {
		for(int i = offset + 1; i < size; i++)
			array[i - 1] = array[i];
	}
}
