package org.toolbox.collections;

public interface Stack<T> extends MutableUnindexedCollection<T> {

	default T push(T e) {
		return add(e);
	}

	T pop();

	T top();
}
