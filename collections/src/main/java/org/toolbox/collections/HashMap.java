package org.toolbox.collections;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class HashMap<K, V> implements MutableIndexedCollection<K, V> {

	private static class Node<NK, NV> {
		private NK key;
		private NV val;

		private Node<NK, NV> next;

		Node(NK key, NV val, Node<NK, NV> next) {
			this.key = key;
			this.val = val;
			this.next = next;
		}

		Node(NK key, NV val) {
			this.key = key;
			this.val = val;
		}
	}

	private Node<K, V>[] table;
	private int size = 0;

	@SuppressWarnings("unchecked")
	public HashMap(int initCap) {
		table = new Node[initCap];
	}

	@Override
	public V get(K key) {
		var n = findNode(key);
		return null == n ? null : n.val;
	}

	@Override
	public K indexOf(V val) {
		for(int i = 0; i < table.length; i++) {

			var n = table[i];
			while(null != n) {
				if(n.val.equals(val))
					return n.key;

				n = n.next;
			}
		}

		return null;
	}

    @Override
    public boolean containsKey(K key) {
		return findNode(key) != null;
    }

	@Override
	public boolean contains(V e) {
		return indexOf(e) != null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void clear() {
		size = 0;
		for(int i = 0; i < table.length; i++)
			table[i] = null;
	}

    @Override
    public V removeAt(K key) {
		int idx = index(key);
		var n = table[idx];

		if(n.key.equals(key)) {
			table[idx] = n.next;
			size--;

			return n.val;
		}

		while(null != n.next) {
			if(n.next.key.equals(key)) {
				var removed = n.next;
				n.next = n.next.next;
				size--;

				return removed.val;
			}

			n = n.next;
		}

		return null;
    }

	@Override
	public V remove(V e) {
		for(int i = 0; i < table.length; i++) {
			var n = table[i];

			if(n != null && n.val.equals(e)) {
				table[i] = n.next;
				size--;

				return n.val;
			}

			while(null != n.next) {
				V val = n.next.val;
				if(val.equals(e)) {
					n.next = n.next.next;
					size--;

					return val;
				}

				n = n.next;
			}
		}

		return null;
	}

	@Override
	public V insert(K key, V val) {
		int idx = index(key);

		if(null == table[idx]) {
			table[idx] = new Node<K, V>(key, val);
			size++;

			return val;
		}

		var n = findNodeAt(key, idx);
		if(null != n)
			return null;

		table[idx] = new Node<>(key, val, table[idx]);

		return table[idx].val;
	}

	@Override
	public V set(K key, V val) {
		var n = findNode(key);
		if(null == n)
			return null;

		n.val = val;

		return val;
	}

	@Override
	public void forEach(Consumer<V> consumer) {
		forEachIndexed((k, v) -> consumer.accept(v));
	}

	@Override
	public void forEachIndexed(BiConsumer<K, V> consumer) {
		for(int i = 0; i < table.length; i++) {
			for(var n = table[i]; n != null; n = n.next)
				consumer.accept(n.key, n.val);
		}
	}


	private Node<K, V> findNode(K key) {
		return findNodeAt(key, index(key));
	}

	private Node<K, V> findNodeAt(K key, int idx) {
		var n = table[idx];

		while(null != n) {
			if(n.key.equals(key))
				return n;

			n = n.next;
		}

		return null;
	}

	private int index(K key) {
		return indexIn(key, table);
	}

	private int indexIn(K key, Node<K, V>[] table) {
		return key.hashCode() % table.length;
	}
}
