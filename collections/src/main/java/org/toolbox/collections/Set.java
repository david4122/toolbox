package org.toolbox.collections;

public interface Set<T> extends UnindexedCollection<T> {}
