package org.toolbox.collections;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class LinkedList<T> implements MutableList<T>, Queue<T>, Stack<T> {

	private static class Node<N> {
		private N val;
		private Node<N> prev;
		private Node<N> next;

		public Node(N val) {
			this.val = val;
		}

		public Node(N val, Node<N> prev) {
			this(val);
			this.prev = prev;
		}
	}

	private Node<T> head;
	private Node<T> tail;

	private int size = 0;

	@Override
	public T add(T e) {
		if(null == e)
			return null;

		if(null == tail) {
			head = tail = new Node<>(e);
		} else {
			tail.next = new Node<>(e, tail);
			tail = tail.next;
		}

		size++;

		return e;
	}

	@Override
	public T insert(Integer key, T val) {
		if(null == val)
			return null;

		if(!containsKey(key)) {
			if(key == size) {
				return add(val);
			}

			return null;
		}

		insertBefore(new Node<>(val), getNode(key));

		size++;

		return val;
	}

	@Override
	public T remove(T e) {
		var n = findNode(e);

		if(null == n)
			return null;

		removeNode(n);

		size--;

		return n.val;
	}

	public T removeAt(Integer key) {
		if(!containsKey(key))
			return null;

		var n = getNode(key);
		removeNode(n);

		size--;

		return n.val;
	}

	@Override
	public boolean containsKey(Integer key) {
		return null != key && key >= 0 && key < size;
	}

	@Override
	public boolean contains(T e) {
		if(null == e)
			return false;

		return indexOf(e) != -1;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void clear() {
		head = tail = null;
		size = 0;
	}

	@Override
	public T get(Integer key) {
		if(!containsKey(key))
			return null;

		return getNode(key).val;
	}

	@Override
	public T set(Integer key, T val) {
		var n = getNode(key);

		if(null == n)
			return null;

		T old = n.val;
		n.val = val;

		return old;
	}

	@Override
	public Integer indexOf(T val) {
		if(null == val)
			return -1;

		var n = head;
		for(int i = 0; null != n; i++) {
			if(n.val.equals(val))
				return i;

			n = n.next;
		}

		return -1;
	}

	@Override
	public T pop() {
		return removeAt(size - 1);
	}

	@Override
	public T poll() {
		return removeAt(0);
	}

	@Override
	public T head() {
		return get(0);
	}

	@Override
	public T top() {
		return get(size - 1);
	}

	@Override
	public void forEach(Consumer<T> consumer) {
		var n = head;
		while(null != n) {
			consumer.accept(n.val);
			n = n.next;
		}
	}

	@Override
	public void forEachIndexed(BiConsumer<Integer, T> consumer) {
		var n = head;
		int i = 0;
		while(null != n) {
			consumer.accept(i++, n.val);
			n = n.next;
		}
	}


	private void insertBefore(Node<T> n, Node<T> next) {
		n.next = next;
		n.prev = next.prev;

		if(null == next.prev)
			head = n;
		else
			next.prev.next = n;
		next.prev = n;
	}

	private void insertAfter(Node<T> n, Node<T> prev) {
		n.prev = prev;
		n.next = prev.next;

		if(null == prev.next)
			tail = n;
		else
			prev.next.prev = n;
		prev.next = n;
	}

	private void removeNode(Node<T> n) {
		if(null == n.prev)
			head = n.next;
		else
			n.prev.next = n.next;

		if(null == n.next)
			tail = n.prev;
		else
			n.next.prev = n.prev;
	}

	private Node<T> getNode(Integer key) {
		if(!containsKey(key))
			return null;

		var n = head;
		for(int i = 0; i < key; i++)
			n = n.next;

		return n;
	}

	private Node<T> findNode(T e) {
		if(null == e)
			return null;

		var n = head;

		while(null != n) {
			if(e.equals(n.val))
				return n;

			n = n.next;
		}

		return null;
	}
}
