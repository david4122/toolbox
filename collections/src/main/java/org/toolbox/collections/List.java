package org.toolbox.collections;

public interface List<T> extends UnindexedCollection<T>, IndexedCollection<Integer, T> {}
