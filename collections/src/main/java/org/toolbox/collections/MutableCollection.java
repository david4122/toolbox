package org.toolbox.collections;

public interface MutableCollection<T> extends Collection<T> {

	void clear();

	T remove(T e);
}
