package org.toolbox.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ListTest {

	private MutableList<Integer> target;

	private static final int ELEMS = 100;

	@BeforeEach
	void setup() {
		// target = new ArrayList<>(10, 1.5);
		target = new LinkedList<>();
	}

	@Test
	void testListSize() {
		assertEquals(0, target.size());

		fillList();

		assertEquals(ELEMS, target.size());

		for(Integer i = 0; i < ELEMS / 2; i++)
			target.remove(i);

		assertEquals(ELEMS - (ELEMS / 2), target.size());

		int s = target.size();

		target.insert(0, -1);
		target.insert(target.size() / 2, -1);
		target.insert(target.size(), -1);

		assertEquals(s + 3, target.size());

		target.clear();

		assertEquals(0, target.size());
	}

	@Test
	void testListContainsAndIndexOf() {
		assertFalse(target.contains(-1));
		assertEquals(-1, target.indexOf(-1));

		fillList();

		assertEquals(0, target.indexOf(0));
		assertEquals(ELEMS - 1, target.indexOf(ELEMS - 1));
		assertEquals(ELEMS / 2, target.indexOf(ELEMS / 2));

		assertTrue(target.contains(0));
		assertTrue(target.contains(ELEMS - 1));
		assertTrue(target.contains(ELEMS / 2));

		assertEquals(-1, target.indexOf(-1));
		assertFalse(target.contains(-1));

		int removed = target.remove(0);
		assertEquals(-1, target.indexOf(removed));
		assertFalse(target.contains(removed));

		removed = target.remove(target.size() - 1);
		assertEquals(-1, target.indexOf(removed));
		assertFalse(target.contains(removed));

		removed = target.remove(target.size() / 2);
		assertEquals(-1, target.indexOf(removed));
		assertFalse(target.contains(removed));

		assertTrue(target.contains(target.get(0)));
	}

	private void fillList() {
		for(int i = 0; i < ELEMS; i++)
			target.add(i);
	}
}
