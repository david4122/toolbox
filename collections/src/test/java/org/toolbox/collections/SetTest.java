package org.toolbox.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SetTest {

	private static final int ELEMS = 100;

	private MutableSet<Integer> target;

	@BeforeEach
	void setup() {
		target = new HashSet<>();
	}

	@Test
	void testContains() {
		assertFalse(target.contains(-1));

		fill();

		IntStream.range(0, ELEMS)
			.boxed()
			.map(target::contains)
			.forEach(i -> assertTrue(i,
						() -> String.format("contains false for item %d", i)));

		assertFalse(target.contains(-1));

		assertFalse(target.contains(null));
	}

	@Test
	void testSize() {
		assertEquals(0, target.size());
		fill();
		assertEquals(ELEMS, target.size());

		target.remove(0);
		assertEquals(ELEMS - 1, target.size());

		target.remove(ELEMS - 1);
		assertEquals(ELEMS - 2, target.size());

		target.remove(ELEMS / 2);
		assertEquals(ELEMS - 3, target.size());

		target.clear();
		assertEquals(0, target.size());
	}

	@Test
	void testIsEmpty() {
		assertTrue(target.isEmpty());
		fill();
		assertFalse(target.isEmpty());

		IntStream.range(1, ELEMS)
			.forEach(target::remove);
		assertFalse(target.isEmpty());

		target.remove(0);
		assertTrue(target.isEmpty());
	}

	@Test
	void testAdd() {
		IntStream.range(0, ELEMS)
			.forEach(i -> assertEquals(i, target.add(i)));

		assertNull(target.add(0));

		target.remove(0);
		assertEquals(0, target.add(0));

		assertNull(target.add(null));
	}

	@Test
	void testRemove() {
		assertNull(target.remove(0));
		assertNull(target.remove(null));

		fill();

		IntStream.range(0, ELEMS)
			.forEach(i -> assertEquals(i, target.remove(i)));

		assertNull(target.remove(0));
	}

	@Test
	void testForEach() {
		MutableList<Integer> added = new ArrayList<>(ELEMS, 2);
		IntStream.range(0, ELEMS)
			.map(target::add)
			.forEach(added::add);

		target.forEach(added::remove);

		assertEquals(0, added.size(), () -> String.format(
					"forEach failed: %d elements skipped: %s",
					added.size(),
					added.toString()));
	}

	private void fill() {
		IntStream.range(0, ELEMS)
			.forEach(target::add);
	}
}
